<?php

/**
 * Template Name: Mntemplate
 */

	remove_action('genesis_site_title', 'genesis_seo_site_title');
	remove_action( 'genesis_site_description', 'genesis_seo_site_description' );
	remove_action('genesis_entry_header', 'genesis_do_post_title');

	add_action('genesis_after_header', 'rm_big_title');
	function rm_big_title(){

		global $wp_query;

		$style = '';
		$text_style = '';

		if(has_post_thumbnail()){
		   $image = get_the_post_thumbnail_url($post->ID, 'full');
		   $style = 'background: url('.$image.') center center no-repeat; color: #fff; background-size: cover; padding: 30px 15px;';
			$text_style = 'color: #fff; text-shadow: 0px 0px 10px rgba(109, 109, 109, 1);';
		}// if thumbnail

		echo '<div class="page_title_container" style="'.$style.'">';
		echo '<div class="page_title_container_inner">';
		echo '<h1 class="page-title" style="'.$text_style.'">'.get_the_title().'</h1>';
		echo '</div>'; // page_title_container_inner
		echo '</div>'; // page_title_container
	}

	remove_action( 'genesis_loop', 'genesis_do_loop' );
  add_action( 'genesis_loop', 'tf_do_custom_loop' );
  function tf_do_custom_loop() {
		global $post;
		?>
		<link href="//cdn.jsdelivr.net/npm/featherlight@1.7.14/release/featherlight.min.css" type="text/css" rel="stylesheet" />
		<script src="//cdn.jsdelivr.net/npm/featherlight@1.7.14/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
		<style>
		h2 { color:#f00028; font-size:20px; padding:0px; margin:0px; padding-bottom:29px; padding-top:29px; }
		.rap_modal h2 { padding-top:15px; padding-bottom:15px; text-align:center; }
		p { font-size:16px; }
		ul, ul li { list-style: disc inside none; }
		ol li::before {
		  content: "";
		  width: 10px;
		  display: inline-block;
		}
		li { font-weight:normal; font-size:16px; padding-bottom:16px;  }
		.site-container .site-inner { margin-top:0px; }
		.toolkit_nav { width:100%; height:104px; }
		.toolkit_nav a { width:50%; text-align:center; float:left;
			font-weight:bold; line-height:104px; font-size:24px;
			background-color:#f2f2f2; color:#252529; }
		.toolkit_nav a.selected, .toolkit_nav a:hover { background-color:#f00028; color:#FFF; }
		.toc, .use_the_toolkit { text-align:center; }

		.toc div, .use_the_toolkit div.rap-tool, #floater div { border: 0px solid; padding: 10px; box-shadow: 0px 5px 10px rgba(0,0,0,.2); border-left:solid 5px #de1c33; border-radius:5px;
		 	margin: 10px auto; max-width:75%; text-align:left; background-color:rgba(255, 255, 255, .5); }
		#floater { width:400px; }
		#floater div { max-width:100%; }
		.toc div.selected, .toc div:hover, .use_the_toolkit div.rap-tool.selected, .use_the_toolkit div.rap-tool:hover,
			#floater div.selected, #floater div:hover
		{ background-color:#de1c33; border-left:solid 5px #000000; color:#FFF; }
		.toc div a, .use_the_toolkit div.rap-tool a, #floater div a { display:block !important; }
		.toc div:hover a, .toc div.selected a, .use_the_toolkit div.rap-tool:hover a, .use_the_toolkit div.rap-tool.selected a,
		 	#floater div:hover a, #floater div.selected a
		{ color:#FFF !important; }
		.toc.floater, #floater {
			z-index:999; background-color:rgba(255, 255, 255, .9);
			position:fixed; top:0px; left:5px;
		}
		.toc.floater div {
		}
		.toc.floater h2, #floater h2 { font-size:16px; padding-bottom:9px; padding-top:9px;  }
		.toc.floater div a, #floater div a { font-size:14px; }
		.use_the_toolkit { margin-bottom:200px; }
		.use_the_toolkit select { width:auto; border-radius:28px; color:#de1c33; border-color:#de1c33; margin-bottom:29px; max-width:90%; z-index:998; padding-right:50px; }
		.rap-tool span { background-color:#de1c33; color:#FFF; font-size:12px; text-transform:uppercase; letter-spacing:1px; border-radius:8px; line-height:12px; margin-top:3px;
		 	padding-top:3px; padding-bottom:3px; padding-left:10px; padding-right:10px; float:right; margin-right:29px;}
		.rap_arrow {
			width:0px; height:0px; border:solid 10px transparent; border-top:solid 10px #de1c33;
			display:inline-block; left:-40px; top:10px; position:relative; }
		.toc-top { position:fixed; bottom:100px; right:15px;}
		.toc-top a {
			display:inline-block; width:52px; height:52px; font-size:40px; line-height:52px;
			background-color:#de1c33; color:#FFF; border-radius:26px; text-align:center;
		}
		.toc-top a:hover {
			opacity:.9;
		}
		.toc-mobile-menu div {
			width:400px; padding:10px; background-color:rgba(255, 255, 255, 1);
		}
		.toc-mobile-menu div a {
			font-size:14px;
		}
		.toc-mobile-menu div:hover {
			background-color:rgba(255, 255, 255, 1);
		}
		.featherlight .featherlight-close-icon {
			font-size:30px; right:5px; top:5px;
		}

		@media (max-width: 700px) {
		 .rap-tool span { float:none; display:block; margin:0px; margin-bottom:5px; text-align:center; }
		 .toolkit_nav a { font-size:16px; }
		}
		</style>
		<script>
		jQuery(window).resize(function() {
			/* var aoff = jQuery('.use_the_toolkit select').offset();
			var ax = aoff.left + jQuery('.use_the_toolkit select').width() + 20;
			jQuery('.rap_arrow').css('left', ax+'px').show(); */
			jQuery(window).scroll();
			resizer();
		});
		jQuery(document).ready(function() {
			resizer();
		});
		function resizer() {
			var whole_width = jQuery('.site-container').width();
			jQuery('.toolkit_nav').css('margin-left','0px').css('width','100%');
			var current_width = jQuery('.toolkit_nav').width();
			var diff = whole_width-current_width;
			if(diff > 0)
			{
				var half_width = Math.floor(diff/2);
				jQuery('.toolkit_nav').css('margin-left', (-1*half_width)+'px').css('width', whole_width+'px');
			}
		}
		var save_toc_height = 0;
		jQuery(document).ready(function() {
			var toc = '';
			var tocc = 0;
			jQuery('.learn_the_toolkit .cont h2').each(function() {
				jQuery(this).attr('id', 'toc'+tocc);
				toc += '<div><a href="#toc'+tocc+'">'+jQuery(this).html()+'</a></div>';
				tocc++;
			});
			var toc_header = ''; //<h2>Risk Assessment &amp; Programming (RAP) Toolkit</h2>';
			jQuery('.learn_the_toolkit .toc').html(toc_header+toc);
			var dt_toc = '<div id="floater" style="display:none;">'+toc+'</div>';
			jQuery(dt_toc).appendTo("body");
			jQuery('<div class="toc-mobile-head" style="display:none;"><input type="button" value="JUMP TO SECTION" /></div>').appendTo("body");
			jQuery('<div class="toc-mobile-menu" style="display:none;">'+toc+'</div>').appendTo("body");
			jQuery('<div class="toc-top" style="display:none;"><a href="#" onclick="jQuery(window).scrollTop(0);return false;">&#8593;</a></div>').appendTo("body");
			save_toc_height = jQuery('.learn_the_toolkit .toc').height();
			jQuery(window).scroll();

		});
		jQuery(window).scroll(function() {

			if(jQuery('#learn_link').hasClass('selected'))
			{
				var scroll = jQuery(document).scrollTop();
				var highlighted = '';
				var found_highlighted = false;
				jQuery('.learn_the_toolkit .cont h2').each(function() {
					var to = jQuery(this).offset();
					if(scroll > (to.top-50)) {
						highlighted = jQuery(this).attr('id');
						found_highlighted = true;
					}
				});
				if(found_highlighted) {
					jQuery('.learn_the_toolkit .toc div, #floater div').each(function() {
						if(jQuery(this).find('a').first().attr('href') == '#'+highlighted) {
							jQuery(this).addClass('selected');
						}
						else jQuery(this).removeClass('selected');
					});
				}

				if(jQuery(window).width() > 1000)
				{
					jQuery('.toc-mobile-head,.toc-mobile-menu').hide();
					jQuery('.cont h2').css('padding-top', '29px');

					var locate = jQuery('.learn_the_toolkit').offset();
					var bottomo = jQuery('.learn_the_toolkit').offset();
					var bottom = bottomo.top+jQuery('.learn_the_toolkit').height();
					if(true) { //locate.top <= scroll) {
						var conto = jQuery('.cont').offset();
						var contx = conto.left;
						jQuery('.cont p, .cont h2, .cont ul, .cont ol').css('padding-left', (425-contx)+'px'); //-contx
						//jQuery('.learn_the_toolkit .toc').addClass('floater');
						//jQuery('.learn_the_toolkit').css('padding-top', save_toc_height+'px');
						//var h = jQuery('.learn_the_toolkit').height();
						jQuery('#floater,.toc-top').show();
						jQuery('.toc').hide();

						var footer_xy = jQuery('.footer-widgets').offset();
						var footer_y = parseInt(footer_xy.top);
						var floater_y = jQuery(window).scrollTop()+jQuery('#floater').height();
						if(footer_y < floater_y) {
							//console.log(footer_y+'-'+floater_y+'='+(footer_y-floater_y));
							jQuery('#floater').css('top', footer_y-floater_y);
						}
						else if(scroll < 650) {
							jQuery('#floater').css('top', (650-scroll)+'px');
						}
						else jQuery('#floater').css('top', '0px');
						/*
						if(bottom < scroll)
						{
							var m = bottom-scroll;
							jQuery('.learn_the_toolkit .toc').css('margin-top', m+'px');
						}
						else {
							jQuery('.learn_the_toolkit .toc').css('margin-top', '0px');
						}
						*/
					}
					else {
						jQuery('.cont p, .cont h2, .cont ul, .cont ol').css('padding-left', '0px');
						//jQuery('.learn_the_toolkit .toc').removeClass('floater');
						jQuery('#floater,.toc-top').hide();
						jQuery('.toc').show();
						jQuery('.learn_the_toolkit').css('padding-top', '0px');
						//jQuery('.learn_the_toolkit .toc').css('margin-top', '0px');
					}

					/*
					jQuery('.toolkit_nav').css('position', 'block').css('margin-top', '0px');
					locate = jQuery('.toolkit_nav').offset();
					if(locate.top <= scroll) {
						jQuery('.toolkit_nav').css('position', 'fixed').css('background-color', '#FFF').css('margin-top', locate.top*-1).css('z-index', 9999);
					}
					*/
				}
				else {
					jQuery('.cont p, .cont h2, .cont ul, .cont ol').css('padding-left', '0px');
					//jQuery('.learn_the_toolkit .toc').removeClass('floater');
					jQuery('#floater,.toc-top').hide();

					jQuery('.toc').show();
					jQuery('.learn_the_toolkit').css('padding-top', '0px');
					//jQuery('.learn_the_toolkit .toc').css('margin-top', '0px');
					jQuery('.toolkit_nav').css('position', 'block').css('margin-top', '0px');
					jQuery('.cont h2').css('padding-top', '100px');

					var locate = jQuery('.learn_the_toolkit').offset();
					var bottomo = jQuery('.learn_the_toolkit').offset();
					var bottom = bottomo.top+jQuery('.learn_the_toolkit').height();
					var start_after_toc = jQuery('.toc').height() + locate.top;
					if(locate.top <= scroll) {
						jQuery('.toc-mobile-head').css('position', 'fixed').css('top','20px').css('z-index', 99999);
						jQuery('.toc-mobile-head,.toc-top').show();
						jQuery('.toc-mobile-head input').unbind();
						jQuery('.toc-mobile-head input').click(function() {
							var currently_shown = (jQuery('.toc-mobile-menu').css('display') != 'none');
							if(currently_shown) {
								jQuery('.toc-mobile-menu').css('display', 'none');
								jQuery('.toc-mobile-head').css('background-color', 'transparent');
							}
							else {
								jQuery('.toc-mobile-menu').css('position', 'fixed').css('top','90px').css('z-index', 99999);
								jQuery('.toc-mobile-menu').show();
								jQuery('.toc-mobile-menu a').unbind();
								jQuery('.toc-mobile-head').css('background-color', '#FFF');
								jQuery('.toc-mobile-menu a').click(function() {
									jQuery('.toc-mobile-menu').css('display', 'none');
									jQuery('.toc-mobile-head').css('background-color', 'transparent');
								});
							}
						});
					}
					else {
						jQuery('.toc-mobile-head,.toc-mobile-menu,.toc-top').hide();
						jQuery('.cont h2').css('padding-top', '29px');
					}

				}
			}
		});
		</script><?php

		echo '<div class="toolkit_nav"><a id="learn_link" href="#" class="selected" onclick="
			jQuery(\'.use_the_toolkit\').hide();
			jQuery(\'.learn_the_toolkit,#floater\').show();
			jQuery(\'#use_link\').removeClass(\'selected\');
			jQuery(this).addClass(\'selected\');
			jQuery(window).resize();
			return false;">Learn the Toolkit</a>
			<a id="use_link" href="#" onclick="
			jQuery(\'.use_the_toolkit\').show();
			jQuery(\'.learn_the_toolkit,#floater\').hide();
			jQuery(\'#learn_link\').removeClass(\'selected\');
			jQuery(this).addClass(\'selected\');
			jQuery(window).resize();
			return false;">Use the Toolkit</a>
			</div><br style="clear:both;height:1px;width:1px;" />';
		echo '<div class="learn_the_toolkit">';
		echo '<h2 style="padding:0px;text-align:center;">Risk Assessment &amp; Programming (RAP) Toolkit</h2>';
		echo '<div class="toc"></div>';
		echo '<div class="cont" style="padding-bottom:500px;">';
		the_content();
		echo '</div>';
		echo '</div>';

		$terms = get_terms( array(
	    'taxonomy' => 'rap_tool_flags',
	    'hide_empty' => true
		) );

		echo '<div class="use_the_toolkit" style="display:none;">
			<h2>RAP Toolkit: Putting It All Together</h2>
			<select name="rap_filter" onchange="
				var chosen = this.options[this.selectedIndex].value;
				jQuery(\'.rap-tool\').each(function() {
					var currently_hidden = (jQuery(this).css(\'display\') == \'none\');
					var should_show = (chosen == \'\');
					if(!should_show) {
						var atags = jQuery(this).data(\'terms\');
						var tags = new Array();
						if(atags.indexOf(\',\') > -1) tags = atags.split(\',\');
						else tags[0] = atags;
						for(var tc=0; tc<tags.length; tc++) {
							if(tags[tc] == chosen) should_show = true;
						}
					}
					if(should_show && currently_hidden) {
						jQuery(this).fadeIn(\'fast\');
					}
					else if(!should_show && !currently_hidden){
						jQuery(this).css(\'display\', \'none\');
					}
				});
			">
				<option value="">All Tools</option>
				<option value="devonly">Tools in Development</option>';
		if(sizeof($terms) > 0) {
			foreach($terms AS $term) {
				echo '<option value="'.$term->term_id.'">'.$term->name.'</option>';
			}
		}
		echo '
			</select><div class="rap_arrow"></div>';
		$lightboxes = '';
		$the_query = new WP_Query(array('post_type' => 'rap_tools', 'posts_per_page' => -1,
			'orderby' => 'title', 'order'   => 'ASC'));
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$id = get_the_ID();
				$tag_ids = array();
				$tags = get_the_terms($id, 'rap_tool_flags');
				if(is_array($tags))
				{
					while(sizeof($tags) > 0) {
						$tag = array_shift($tags);
						array_push($tag_ids, $tag->term_id);
					}
				}
				$fields = get_fields();
				if(array_key_exists('under_development', $fields) && $fields['under_development'] == 1) {
					array_push($tag_ids, 'devonly');
				}
				echo '<div class="rap-tool" data-terms=",'.implode(',', $tag_ids).'">';
				echo '<a href="#" data-featherlight="#toolmodal'.$id.'">';
				if(array_key_exists('under_development', $fields) && $fields['under_development'] == 1) {
					echo '<span>In Development</span>';
				}
				the_title();
				echo '</a>';
				echo '</div>';
				$lightboxes .= '<div class="rap_modal" id="toolmodal'.$id.'" style="clear:both;">';
				$lightboxes .= '<h2>';
				$lightboxes .= get_the_title();
				$lightboxes .= '</h2><div class="c">';
				$lightboxes .= get_the_content();
				$lightboxes .= '</div></div>';
			}
			wp_reset_postdata();
			echo '<div style="display:none;">'.$lightboxes.'</div>';
		}
		else {
			echo '<p>No RAP Tools Found</p>';
		}
		echo '</div>';
	}

	genesis();
