<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thetoni1_WPD7T');

/** MySQL database username */
define('DB_USER', 'thetoni1_WPD7T');

/** MySQL database password */
define('DB_PASSWORD', 'YNyyGL2XuBbUOauyh');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '0fb76a5ce54060c92a0cc64832135fa2ad025ff0d4f42910f1987501b0e52c18');
define('SECURE_AUTH_KEY', '0158ac5bf2e31e0d7a26dda1c80916c99e8f9507199b61cff45a1b54ffb617bc');
define('LOGGED_IN_KEY', '716602f3551f5c9aec572d47a6efd0cd89241ebe45aa42f339b8ab202f633f32');
define('NONCE_KEY', 'a61ec32b87059122f9d6a838946ecd783e641fec80052564dea1b8fbc990b069');
define('AUTH_SALT', '4e871a5a3833af046ad0dde5c19a2d830fa992d8db811da27ba93116113f3fad');
define('SECURE_AUTH_SALT', '5ace651731d2ff5a07ac84950bb5f9f7011770233cd75c483f10b69488eb0f50');
define('LOGGED_IN_SALT', 'bcc5a220db3cf60be04cf83fab6801cc7442d3ca7e3dcb5ba39c9583e86ac2a3');
define('NONCE_SALT', '2de63cc57f807be850842bc4516dee700c3a343d9ff0b7d83f353c7d6efc279d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '_D7T_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


// Settings modified by hosting provider
define( 'WP_CRON_LOCK_TIMEOUT', 120   );
define( 'AUTOSAVE_INTERVAL',    300   );
define( 'WP_POST_REVISIONS',    5     );
define( 'EMPTY_TRASH_DAYS',     7     );
define( 'WP_AUTO_UPDATE_CORE',  true  );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

